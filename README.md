This project is simply a pre-configured instance of grafana for use with X-Ray (www.nutanix.com/xray) or Curie (https://gitlab.com/nutanix/curie)

# Getting started
On your X-Ray VM, if it's not already running:
`docker run -d -p 3000:3000 --network host --name xray-grafana registry.gitlab.com/nutanix/xray-grafana:latest`

Or; just deploy like any other container.
